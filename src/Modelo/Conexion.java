/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class Conexion {

    private Connection conect = null;

    public Connection conectar() {
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conect = DriverManager.getConnection("jdbc:mysql://localhost/almacen_proyecto","root","");
            

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println("conexion realizada");
        }
        return conect;

    }
    
    
      public void desconectar() throws SQLException {
        conect = null;
        if (conect != null) {
            JOptionPane.showMessageDialog(null, "No se puedo cerrar la conexion a la base de datos");
        }
    }
}
