/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author miguel
 */
public class Cliente extends Persona {
    
 
    private int telefono;
    private String email;

    public Cliente( int identificacion, String nombre, String apellido,int telefono, String email)  throws Exception{
        super(identificacion, nombre, apellido);
        this.telefono = telefono;
        this.email = email;
    }

    public Cliente(int identificacion, String nombre, String apellido) {
        super(identificacion, nombre, apellido);
    }

  


    
    

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

   
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

 
   

   

   

   
    
}
