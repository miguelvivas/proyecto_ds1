/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author miguel
 */
public class Empleado extends Persona{
    
    private short codigo;
    private String ciudadresidencia;
    private long telefono;

    public Empleado(short codigo, String ciudadresidencia, long telefono, int identificacion, String nombre, String apellido) {
        super(identificacion, nombre, apellido);
        this.codigo = codigo;
        this.ciudadresidencia = ciudadresidencia;
        this.telefono = telefono;
    }

    public short getCodigo() {
        return codigo;
    }

    public void setCodigo(short codigo) {
        this.codigo = codigo;
    }

    public String getCiudadresidencia() {
        return ciudadresidencia;
    }

    public void setCiudadresidencia(String ciudadresidencia) {
        this.ciudadresidencia = ciudadresidencia;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

   


   

  
    
}
