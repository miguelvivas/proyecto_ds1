/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Dao.ProductoDao;
import Modelo.Conexion;
import Modelo.Producto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class ProductoController implements ProductoDao {

    Conexion cc = new Conexion();
    Connection cn = cc.conectar();
    String sSql = " ";
    ResultSet rs;
    Statement st;

    @Override
    public boolean registrar(Producto producto) {
        //por cada campo en el parentesis values colocar un signo de pregunta ? separado de una coma
        sSql = "insert into producto (codigo, nombre,descripcion,precio, iva, cantidad) values (?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement pst = cn.prepareStatement(sSql);
            pst.setInt(1, producto.getCodigo());
            pst.setString(2, producto.getNombre());
            pst.setString(3, producto.getDescripcion());
            pst.setInt(4, producto.getPrecio());
            pst.setInt(5, producto.getIva());
            pst.setInt(6, producto.getCantidad());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public boolean eliminar(int codigo) {
        sSql = "delete from producto where codigo = '" + codigo + "'";

        try {

            PreparedStatement pst = cn.prepareStatement(sSql);

            int i = pst.executeUpdate();

            if (i != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println("Producto eliminado ");
            return false;
        }

    }

    @Override
    public ResultSet BuscarCliente(int codigo) {

        sSql = "select * from producto where codigo = '" + codigo + "' ";
        try {
            st = cn.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            rs = st.executeQuery(sSql);
        } catch (SQLException ex) {
            Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;

    }

    @Override
    public boolean actualizar(int codigo, String nombre, String descripcion, int precio, int iva, int cantidad) {
        sSql = "update producto set nombre ='" + nombre + "',iva='" + iva + "',descripcion='"+descripcion +
                "',precio='"+precio+"',cantidad='"+cantidad+"'where codigo =" + codigo;

        try {

            PreparedStatement pst = cn.prepareStatement(sSql);

            int i = pst.executeUpdate();

            if (i != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println("Producto actualizado");
            return false;
        }

    }
}
