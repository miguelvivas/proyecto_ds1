/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelo.Cliente;
import java.sql.ResultSet;

/**
 *
 * @author miguel
 */
public interface ClienteDao {

    public boolean registrar(Cliente cliente);

    public ResultSet BuscarCliente(int identificacion);

    public boolean actualizar(long telefono, String email, int identificacion, String nombre, String apellido);

    public boolean eliminar(int identificacion);

}
