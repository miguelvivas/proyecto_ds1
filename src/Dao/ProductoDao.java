/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelo.Conexion;
import Modelo.Producto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public interface  ProductoDao {
    
    public boolean registrar(Producto producto);
    public ResultSet BuscarCliente(int codigo);
   public boolean actualizar(int codigo, String nombre, String descripcion, int precio, int iva, int cantidad);
    public boolean eliminar(int codigo);
   
    
    
    
}
